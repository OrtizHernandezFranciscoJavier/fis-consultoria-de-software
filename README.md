# Mapa 1
```plantuml
@startmindmap
*[#lightPink] La consultoría \n "Una profesion de exito: Presente y futuro"
	*[#Orange] ¿Que es la consultoria?
		*[#Green] La Consultoria nace porque existen grandes organizaciones\n (tipicos clientes) que internamente tienen mucha complejidad \n en sus procesos internos requieren de sistemas de informacion que les\n  den soporte a esos procesos que son tremendamente complejos.
			*[#lightgreen] El mercado obliga a las empresas a evolucionar \n si las empresas no evolucionan estas desaparecen \n Tienen que estar en una constante renovacion de procesos \nla competencia obliga a esto al mismo tiempo tienen que estar\n operando a lo que se dedican. habitualmente \n no tienen la capacidad interna para hacer las dos \n cosas a la vez. 
				*[#lightyellow] Como no pueden hacer las dos cosas a la vez recurren a \n empresas que les ayudan a transformarse o a operar su\n negocio actual. \n Aqui es donde nace la consultoria. 
			*[#lightgreen] Prestacion de Servicios de alto valor añadido que relizan \n las empresas consultoras. \nHabitualmente tienen tres caracteristicas fundamentales:
				*[#lightyellow] *Necesitan tener un conocimiento sectorial importante  \n*Se requiere un conocimiento tecnico importante muchas veces a la vanguardia \n*Capacidad de plantear soluciones a los problemas del mundo empresarial.
			*[#lightgreen] AXPE Consulting\n Es una compañia multinacional de capital 100% español,\n de consultoria y tecnologias de informacion, especializada en \n servicios de outsourcing, consultoria y ejecucion de proyectos \n cerrados.
				*[#lightyellow] Nacio en españa en 1998 \nCompañia independiente, con gran solidez financiera que da \n servicio a las compañias del ibex35 y que distribuye su facturacion\n entre mas de 150 clientes \n* Mas de 1600 profesionales a nivel mundial trabajan en: \n Europa: 1300 empleados en España (7 centros), UK y Francia \n*LATAM: \nMexico con un equipo de 220 profesionales \n con presencia en Peru, Colombia ,Brasil,Panama \n Republica Dominicana y Argentina. \n*Facturacion global del grupo en 2014 fue de \n 70M Euros de los cuales 62M euros en España.    
	*[#Orange] ¿Que tipos de servicios presta?
		*[#GREEN] Consultoria
			*[#lightgreen] Reingenieria de Procesos		
				*[#lightyellow] Se desarrollan actividades que tienen que ver con la ingeniería o reingeniería de \n procesos en la empresa es decir cómo ayudar a las empresas a que organicen \n  sus actividades siguiendo un determinado esquema de funcionamiento de \n manera que sea más óptimo qué el que tienen en la actualidad atendiendo a los \n  cambios que suele haber con cierta periodicidad   
			*[#lightgreen] Gobierno Ti
				*[#lightyellow] Hay una consultoría específicamente dirigida CEO, al responsable de la \n organización la que suele llamarse de gobierno TI que está muy ligada \n a un conjunto de mejores prácticas 
					*[#lightblue] Dichas practicas pueden tener que ver con:\n*la calidad \n*metodologías de desarrollo \n*arquitectura empresarial 
			*[#lightgreen] Oficina de proyectos
				*[#lightyellow] Se presta apoyo a las empresas para que gestionen correctamente los proyectos \n organizacionales que llevan a cabo 
			*[#lightgreen] Analitica avanzada de datos
				*[#lightyellow] Hoy en día la información es crucial para para las empresas, es muy importante \n que dispongan en tiempo real de la información que les permita tomar decisiones.
					*[#lightblue] Actualmente se genera muchísima más información de la que todavía tenemos capacidad \n para procesar y obtener conocimientos
		*[#GREEN] Integracion
			*[#lightgreen] Desarrollos a medida
				*[#lightyellow] Toda empresa tiene un mapa de sistema con el que da soporte a su \n actividad de negocio este por un lado habla de piezas software y por \n otro lado habla de piezas Hardware o de infraestructura y  \n comunicaciones dicho mapa requiere evolución continua. 
					*[#lightblue] El ambito de la integracion se dedica a ir metiendo piezas de software\n o del Hardware o de ambas cosas en el mapa de sistemas de las compañías  
			*[#lightgreen] Aseguramiento de la calidad de \n software
				*[#lightyellow] Los procesos de negocio suelen tener La mala costumbre de ser de \nextremo a extremo y por tanto tienen que intervenir en su solución \ndiferentes sistemas y asegurar que cuando un proceso negocio se ve \nafectado, se realiza su plantación de manera adecuada y con la calidad \n necesaria extremo a extremo suele requerir de poner proyectos \n específicamente dirigidos y presionarlos. 	
			*[#lightgreen] Infraestructura
				*[#lightyellow] También hay servicios de infraestructura que hoy día pues tiene mucho\n que ver con la definición del uso de nuevas tecnologías 
			*[#lightgreen] Soluciones de mercado
				*[#lightyellow] Las empresas suelen desarrollar sus sistemas de soporte a su operación\ny a su negocio basándose en Software pre construido y\nparametrizable son soluciones de mercado ya existentes que requieren\nun conocimiento profundo de la propia solución para entender\nlo que demanda el cliente lo que demanda la problemática\nque nos plantea el cliente trasladar esa parametrización a los\nsistemas las soluciones ya pre construidas para ofrecerle una solución\nque les sea viable. 
		*[#GREEN] Externalizacion
			*[#lightgreen] Gestion de aplicaciones 
				*[#lightyellow] Esta se da cuando las empresas deciden delegar todo el mantenimiento\n y evaluación de sus sistemas en un tercero lo regula por un acuerdo de\n nivel de servicio parametrizado por complejidades, por diferentes tipos\n de medidas o de métricas lo cual lo establece por varios años.
			*[#lightgreen] Servicios SQA
			*[#lightgreen] Administracion y operacion de \nInfraestructura
			*[#lightgreen] Procesos de negocio
				*[#lightyellow] Externalizar procesos completos de negocios asumiendo no solo la parte\n Ti sino también la operación del negocio lo que se llaman los\n BPOs (Business Process Outsourcing). 
				 		   
			
		      
 
@endmindmap
```
# Mapa 2
```plantuml
@startmindmap
*[#Pink] Consultoría de Software
	
@endmindmap
```
# Mapa 3
```plantuml
@startmindmap
*[#Pink] Aplicación de la ingeniería de software
	
@endmindmap